namespace Lib.Infra.CrossCutting.Identity.Authorization
{
    public class TokenDescriptor
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int MinutesValid { get; set; }
    }
}
